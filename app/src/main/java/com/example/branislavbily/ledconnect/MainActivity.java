package com.example.branislavbily.ledconnect;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //TODO Create own Alert dialog with buttons in theme

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void changeToConnectActivity(View view) {
        Intent intent = new Intent(this, DeviceListActivity.class);
        startActivity(intent);
    }

    public void onExitButtonClicked(View view) {
        final AlertDialog alertDialog = createAlertDialog();

        TextView title = createTitle();
        alertDialog.setCustomTitle(title);

        TextView msg = createMessage();
        alertDialog.setView(msg);

        setNeutralButton(alertDialog);
        setNegativeButton(alertDialog);

        new Dialog(getApplicationContext());
        alertDialog.show();

        final Button okBT = createButtonOk(alertDialog);

        createButtonNegative(alertDialog, okBT);
    }

    private AlertDialog createAlertDialog() {
        return new AlertDialog.Builder(this).create();
    }

    private void setNeutralButton(AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL,"YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
    }

    private void setNegativeButton(final AlertDialog alertDialog) {
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
    }

    private TextView createTitle() {
        TextView title = new TextView(this);
        title.setText(R.string.alertDialogTitle);
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.BLACK);
        title.setTextSize(25);
        return title;
    }

    private TextView createMessage() {
        TextView msg = new TextView(this);
        msg.setText(R.string.alertDialogMessage);
        msg.setGravity(Gravity.CENTER_HORIZONTAL);
        msg.setTextColor(Color.BLACK);
        msg.setTextSize(20);
        return msg;
    }

    private Button createButtonOk(AlertDialog alertDialog) {
        Button okBT = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        LinearLayout.LayoutParams neutralBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        okBT.setBackgroundResource(R.drawable.alertdialogbuttonpositive);
        neutralBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        okBT.setTextColor(Color.WHITE);
        okBT.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        okBT.setTextSize(20);
        okBT.setLayoutParams(neutralBtnLP);
        return okBT;
    }

    private void createButtonNegative(AlertDialog alertDialog, Button okBT) {
        Button cancelBT = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        LinearLayout.LayoutParams negBtnLP = (LinearLayout.LayoutParams) okBT.getLayoutParams();
        cancelBT.setBackgroundResource(R.drawable.alertdialogbuttonnegative);
        negBtnLP.gravity = Gravity.FILL_HORIZONTAL;
        cancelBT.setTextColor(Color.WHITE);
        cancelBT.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        cancelBT.setTextSize(20);
        cancelBT.setLayoutParams(negBtnLP);
    }
}
