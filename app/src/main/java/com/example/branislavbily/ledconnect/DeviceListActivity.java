package com.example.branislavbily.ledconnect;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class DeviceListActivity extends AppCompatActivity {

    Button pairButton;
    ListView deviceList;

    private BluetoothAdapter myBluetooth = null;
    public static String EXTRA_ADDRESS = "device_adreess";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        pairButton = findViewById(R.id.pairButton);
        deviceList = findViewById(R.id.deviceList);

        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null) {
            Toast.makeText(this, "Bluetooth Device not available", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            if(myBluetooth.isEnabled()) {} {
                Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
                startActivity(discoverableIntent);
            }
        }
    }

    public void onPairedDevicesButtonClicked(View view) {
        pairedDevicesList();
    }

    private void pairedDevicesList() {
        Set<BluetoothDevice> pairedDevices;
        pairedDevices = myBluetooth.getBondedDevices();
        ArrayList <String> list = new ArrayList<String>();

        if(pairedDevices.size() > 0) {
            for (BluetoothDevice bt :
                    pairedDevices) {
                list.add(bt.getName() + "\n" + bt.getAddress());
            }
        } else {
            Toast.makeText(this, "No paired bluetooth devices found", Toast.LENGTH_SHORT).show();
        }

        ArrayAdapter<String> adapter = createArrayAdapter(list);
        deviceList.setAdapter(adapter);
        deviceList.setOnItemClickListener(myClickListener);
        Toast.makeText(this, "Showing all paired devices", Toast.LENGTH_SHORT).show();
    }

    private AdapterView.OnItemClickListener myClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String info = ((TextView)view).getText().toString();
            //MAC Address is located in the last 17 chars in the View
            String address = info.substring(info.length() - 17);
            Intent intent = new Intent(DeviceListActivity.this, LedControlActivity.class);
            intent.putExtra(EXTRA_ADDRESS, address);
            startActivity(intent);
        }
    };

    private ArrayAdapter<String> createArrayAdapter(ArrayList list) {
        return new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
    }

    public void message(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }
}
