package com.example.branislavbily.ledconnect;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

public class LedControlActivity extends AppCompatActivity {

    Button buttonDisconnect;
    Switch toggleSwitch;

    String address;

    ProgressBar progressBar;
    private BluetoothAdapter myBluetooth = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outputStream = null;

    SeekBar brightnessSeekBar;

    EditText editText;

    ImageView diode;

    boolean diodeTurnedOn;
    private boolean isBtConnected;

    float diodeBrightness;

    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led_control);

        Intent newInt = getIntent();
        address = newInt.getStringExtra(DeviceListActivity.EXTRA_ADDRESS);

        toggleSwitch = findViewById(R.id.toggleSwitch);

        diodeTurnedOn = false;

        progressBar = findViewById(R.id.progressBar);
        buttonDisconnect = findViewById(R.id.buttonDisconnect);

        brightnessSeekBar = findViewById(R.id.seekBarBrightness);
        brightnessSeekBar.setMax(255);

        diode = findViewById(R.id.diodeOn);

        editText = findViewById(R.id.editText);

        toggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(toggleSwitch.isChecked() && isBtConnected) {
                    sendData((int) diodeBrightness);
                    diode.setAlpha(diodeBrightness / 255);
                    Log.i("Hej", "onCheckedChanged: " + diodeBrightness / 255);
                    diodeTurnedOn = true;
                } else if (isBtConnected) {
                    sendData(0);
                    diode.setAlpha((float) 0);
                    diodeTurnedOn = false;
                } else {
                    message("You are not connected to any bluetooth device");
                }
            }
        });

        new ConnectBT().execute();

        brightnessSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                diodeBrightness = (float) progress;
                String progressString = Integer.toString(progress);
                editText.setText(progressString);
                editText.setSelection(String.valueOf(progress).length());
                if(diodeTurnedOn) {
                    diode.setAlpha(diodeBrightness / 255);
                    //sendData((int) diodeBrightness);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if(seekBar.getProgress() == 0) {
                    toggleSwitch.setChecked(false);
                } else if(diodeTurnedOn) {
                    sendData((int) diodeBrightness);
                }
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    int brightness = Integer.parseInt(s.toString());
                    if(brightness <= 255) {
                        brightnessSeekBar.setProgress(brightness);
                    } else {
                        String maxProgress = Integer.toString(255);
                        editText.setText(maxProgress);
                    }
                } catch (Exception e) {}
            }
        });

        //TODO Add activity which chooses what to do with Arduino
    }
    @Override
    protected void onStop() {
        super.onStop();
        disconnect(buttonDisconnect);
    }

    public void sendData(int status) {
        byte[] msgBuffer = Integer.toString(status).getBytes();

        Log.d("Fragment activity", "Send data: " + status + "...");

        try {
            outputStream.write(msgBuffer);
        } catch (IOException e) {
            Log.e("Send data", "" + e.getMessage());
        }
    }

    public void disconnect(View view) {
        if(btSocket != null) {
            try {
                sendData(0);
                btSocket.close();
                finish();
            } catch (IOException e) {
                message("Error");
            } catch (NullPointerException e) {}
        }
    }

    private void message(String s) {
        Toast.makeText(LedControlActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    private void errorExit(String title, String message){
        Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
        finish();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        private boolean connectSuccess = true;

        @Override
        protected void onPreExecute() {
            progressBar.animate();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if (btSocket == null || !isBtConnected)  {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice connection = myBluetooth.getRemoteDevice(address);
                    btSocket = connection.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();
                }
            } catch (IOException e) {
                connectSuccess = false;
                Log.e("Connection", "doInBackground: ", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            if (!connectSuccess) {
                message("Connection failed.");
            } else {
                message("Connected.");
                isBtConnected = true;
                try {
                    outputStream = btSocket.getOutputStream();
                } catch (IOException e) {
                    errorExit("Fatal error", "Output stream creation failed " + e.getMessage() + ".");
                }
            }
        }
    }

}
