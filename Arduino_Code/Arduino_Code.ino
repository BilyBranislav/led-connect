int LED = 13;
int pwm = 9;
int incomingByte;
int bright;
String brightness = "";

#include <SoftwareSerial.h> 
SoftwareSerial mySerial(10, 11); //rx tx

void setup(){
  mySerial.begin(9600);
  Serial.begin(4800);
  pinMode(LED, OUTPUT);
}
void loop() {
  brightness = "";
  if (mySerial.available()) {
    delay(20);
    while(mySerial.available()) {
      incomingByte = mySerial.read() - 48; //ascii
      brightness.concat(incomingByte);
    }
    if(brightness == "0") {
       digitalWrite(LED, LOW); 
    } else {
      digitalWrite(LED, HIGH);
      bright = brightness.toInt();
      digitalWrite(pwm, bright);
      Serial.println(bright);
    }
  }
}
